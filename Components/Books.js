import React from 'react';
import {View, FlatList, TouchableOpacity, Text, StyleSheet} from 'react-native';
import AddButton from './AddButton';

import {myFirebase} from '../firebase';

class Books extends React.Component {
  static navigationOptions = {
    title: 'Номууд',
  };
  navigate = this.props.navigation.navigate;

  state = {
    books: [],
  };

  componentDidMount() {
    this.fetchBookFromFirebase().then(r => console.log('Amjilttai'));
  }
  async fetchBookFromFirebase() {
    try {
      await myFirebase.database.ref('/books').on('value', snapshot => {
        if (snapshot.val() == null) {
          this.setState({books: []})
          return;
        }
        this.setState({
          books: Object.keys(snapshot.val()).map(key => {
            const book = snapshot.val()[key];
            return {
              key,
              name: book.name,
              author: book.author,
              year: book.year,
              pages: book.pages,
            };
          }),
        });
      });
    } catch (e) {
      console.log(e);
    }
  }

  addBook = () => this.navigate('Book');
  handlePress = book => this.navigate('Book', {book});

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.books}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => this.handlePress(item)}>
              <Text style={styles.item}>{'Нэр: ' + item.name + ' - Зохиолч: ' + item.author}</Text>
            </TouchableOpacity>
          )}
        />
        <AddButton press={this.addBook} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

export default Books;

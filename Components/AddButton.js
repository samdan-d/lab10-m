import React from 'react';
import {Button} from 'react-native';

const AddButton = ({press}) => <Button onPress={press} title={'Нэмэх'} />;

export default AddButton;

import React, {useState} from 'react';
import {View, Button, TextInput} from 'react-native';
import {myFirebase} from '../firebase';

const Book = ({navigation}) => {
  const save = !navigation.getParam('book', false);
  const [book, setBook] = useState(
    navigation.getParam('book', {
      name: '',
      author: '',
      year: 0,
      pages: 0,
    }),
  );

  const nameChange = e => setBook({...book, name: e.nativeEvent.text});
  const authorChange = e => setBook({...book, author: e.nativeEvent.text});
  const yearChange = e => setBook({...book, year: e.nativeEvent.text});
  const pagesChange = e => setBook({...book, pages: e.nativeEvent.text});

  const saveOrUpdate = async () => {
    try {
      if (save) {
        await myFirebase.database.ref('/books').push({...book});
        navigation.goBack();
      } else {
        await myFirebase.database.ref(`/books/${book.key}`).set({...book});
        navigation.goBack();
      }
    } catch (e) {
      throw e + 'error save or update!';
    }
  };

  const deletePressHandle = async () => {
    try {
      await myFirebase.database.ref(`/books/${book.key}`).remove();
      navigation.goBack();
    } catch (e) {
      throw e + 'error remove!';
    }
  };

  return (
    <View>
      <TextInput placeholder="Нэр" value={book.name} onChange={nameChange} />
      <TextInput
        placeholder="Зохиолч"
        value={book.author}
        onChange={authorChange}
      />
      <TextInput
        placeholder="Хэвлэсэн жил"
        value={book.year}
        keyboardType="number-pad"
        onChange={yearChange}
      />
      <TextInput
        placeholder="Хуудас"
        value={book.pages}
        keyboardType="number-pad"
        onChange={pagesChange}
      />
      <Button onPress={saveOrUpdate} title={save ? 'Save' : 'Update'} />
      {save || (
        <Button color="red" onPress={deletePressHandle} title="Delete" />
      )}
    </View>
  );
};

Book.navigationOptions = {
  title: 'Ном',
};

export default Book;
